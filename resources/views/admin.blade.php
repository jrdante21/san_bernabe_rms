@extends('layouts.app')
@section('title', 'San Bernabe RMS')
@php
    if (auth()->check()) {
        $user = auth()->user();
    } elseif (auth()->guard('official')->check()) {
        $user = auth()->guard('official')->user();
    } else {
        $user = null;
    }
@endphp
@section('content')
    <div id="app"></div>
    <script>
        window.$documentTypes = @json($documentTypes);
        window.$officialPositions = @json($officialPositions);
        window.$user = @json($user);
    </script>
@endsection
@section('scripts')
    <script src="{{ mix('js/app.js') }}" defer></script>
@endsection