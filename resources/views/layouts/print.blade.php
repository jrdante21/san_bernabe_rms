<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="icon" href="/images/logo.png">
    <title>@yield('title')</title>
</head>
<style>
    html, body {
        font-family: Arial, Helvetica, sans-serif;
        line-height: 1.5;
        font-size: 13.5px;
    }
    .text-center { text-align: center; }
    .text-lg { font-size: 15px; }
    .text-xl { font-size: 16px; }
    .collapse { width: 1px; white-space: nowrap; }
    .uppercase { text-transform: uppercase; }
    .lowercase { text-transform: lowercase; }
    .underline { position: relative; }
    .underline::after {
        content: '';
        width: 100%;
        position: absolute;
        left: 0;
        bottom: -3px;
        border-width: 0 0 1px;
        border-style: solid;
    }
    .indent { text-indent: 30px; }
    .flex-end {
        display: flex;
        flex-direction: row;
        justify-content: flex-end;
    }
    .flex-center {
        display: flex;
        flex-direction: row;
        justify-content: center;
    }
    .flex {
        display: flex;
        flex-direction: row;
        gap: 10px;
        align-items: center;
    }
    .flex-col {
        display: flex;
        flex-direction: column;
        gap: 8px;
    }
    .table {
        width: 100%;
        border-collapse: collapse;
    }
    .table td, .table th {
        border: 1px solid #ccc;
        padding: 2px 5px;
    }
    .table th { background-color: #ebebeb; }
    .border-bottom { border-bottom: solid 1px #ccc; }
    .mb-5px { margin-bottom: 5px; }
</style>
<body onload="printThis()">
    
    <table class="text-lg" width="100%">
		<tr>
			<td style="border: 0px; width: 25%; text-align: center"><img src="/images/logo1.png" style="width: 80px;"></td>
			<td style="border: 0px; width: 50%;" align="center">
                <div>Republic of the Philippines</div>
                <div><b class="uppercase">Province of Quirino</b></div>
                <div>Municipality of Maddela</div>
                <div><b class="uppercase">Barangay San Bernabe</b></div>
            </td>
			<td style="border: 0px; width: 25%; text-align: center"><img src="/images/logo.png" style="width: 80px;"></td>
		</tr>
	</table>
    <div class="uppercase text-center text-xl" style="margin-top: 20px;"><b>@yield('heading')</b></div>
    <div style="margin-top: 20px;">
        @yield('content')
    </div>

    <script>
        function printThis() {
            window.print()
            setTimeout(function(){
                window.close()
            }, 1000)
        }
    </script>
</body>
</html>