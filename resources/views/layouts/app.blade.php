<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{ mix('css/app.css') }}">
    <link rel="icon" href="/images/logo.png">
    <meta name="naive-ui-style" />
    @yield('scripts')
    <title>@yield('title')</title>
    <style>
        .n-layout, .n-layout-header, .n-layout-content, .n-layout-sider {
            background: transparent !important;
        }
        td {
            background-color: transparent !important;
        }
    </style>
</head>
<body>
    @yield('content')
</body>
</html>