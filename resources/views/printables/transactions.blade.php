@extends('layouts.print')
@section('title', 'Transaction Report')
@section('heading', 'Transaction Report')
@section('content')
    <table class="table">
        <thead>
            <tr>
                <th class="collapse">Receipt No.</th>
                <th>Resident Name</th>
                <th>Type</th>
                <th class="collapse">CTC No.</th>
                <th class="collapse">Amount</th>
                <th class="collapse">Date Issued</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($receipts as $receipt)
            <tr>
                <td class="collapse">{{ $receipt->receiptable->or_number }}</td>
                <td>{{ $receipt->resident->fullname }}</td>
                <td>{{ $receipt->receiptable->title }}</td>
                <td class="collapse">
                    @if (!empty($receipt->receiptable->ctc_number))
                        {{ $receipt->receiptable->ctc_number }}
                    @else
                        <i>N/A</i>
                    @endif
                </td>
                <td class="collapse" align="right">{{ number_format($receipt->receiptable->amount, 2) }}</td>
                <td class="collapse">{{ now()->parse($receipt->receiptable->issued_at)->format('F d, Y') }}</td>
            </tr>
            @endforeach
            <tr>
                <td colspan="4" align="right"><b>Total</b></td>
                <td align="right"><b>{{ number_format($total, 2) }}</b></td>
                <td></td>
            </tr>
        </tbody>
    </table>
@endsection