<div style="margin-top: 30px; font-weight: bold;">
    <div>OR No. {{ $document->or_number }}</div>
    <div>CTC No. {{ $document->ctc_number }}</div>
    <div>Date Issued: {{ date('F d, Y', strtotime($document->issued_at)) }}</div>
</div>