<table class="table">
    <thead>
        <tr>
            <th>#</th>
            <th>Resident Name</th>
            <th>Address</th>
            @if ($withAssets)
                <th>Asset</th>
            @endif
            <th>Date Registered</th>
        </tr>
    </thead>
    <tbody>
        @php
            $n = 1;
        @endphp
        @foreach ($residents as $resident)
            <tr>
                <td class="collapse">{{ $n++ }}.</td>
                <td>{{ $resident->fullname }}</td>
                <td>{{ $resident->address }}</td>
                @if ($withAssets)
                    <td>
                        @if (count($resident->assets) >= 1)
                            <div class="flex-col">
                                @foreach ($resident->assets as $asset)
                                    <div>
                                        <div><b>{{ $asset->type_str }}</b></div>
                                        <div class="uppercase" style="padding-left: 10px;">
                                            @foreach ($asset->details as $detail)
                                                <div>{{ $detail['label'] }}: <b>{{ $detail['value'] }}</b></div>
                                            @endforeach
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        @else
                            N / A
                        @endif
                    </td>
                @endif
                <td class="collapse">{{ now()->parse($resident->created_at)->format('F d, Y') }}</td>
            </tr>
        @endforeach
    </tbody>
</table>