@extends('layouts.print')
@section('title', $title)
@section('heading', 'Office of the Punong Barangay')
@section('content')
    {{-- Title --}}
    <div class="uppercase text-center text-xl" style="margin: 20px 0px;"><b>{{ $title }}</b></div>

    <p><b class="uppercase">To whom it may concern:</b></p>

    @switch($document->type)
        {{-- Business Permit --}}
        @case(5)
            <p class="indent">
                <b>THIS IS TO CERTIFY that I officially know <span class="underline">{{ $document->resident->fullname }}</span></b>,
                of legal age, single and owner of <b class="underline">{{ $document->residentBusiness->name }}</b> at 
                {{ $document->residentBusiness->address_str }} is given permit to operate his/her establishment/business subject
                however to the provision of our existing laws and ordinance, and rules & regulation governing the operation and maintenance of the same.
            </p>
            <p class="indent">
                This permit may be revoked at any time when necessary to protect the public interest or violation
                of any provision
            </p>
            @break

        {{-- Business Clearance --}}
        @case(6)
            <p class="indent">
                <b>THIS IS TO CERTIFY that I officially know <span class="underline">{{ $document->resident->fullname }}</span></b>,
                resident of Barangay San Bernabe, Maddela, Quirino. Has no pending obligation in our Purok.
            </p>
            <p>
                <div class="flex-center text-center" style="gap: 30px">
                    <div>Owner of:</div>
                    <div>
                        <div><b class="underline">{{ $document->residentBusiness->name }}</b></div>
                        <div><i>Name type of Business</i></div>
                    </div>
                </div>
            </p>
            <p>
                <div class="flex-center text-center">
                    <div>
                        <div><b class="underline">{{ $document->residentBusiness->address_str }}</b></div>
                        <div><i>Place of Business</i></div>
                    </div>
                </div>
            </p>
            <p class="indent">
                Provided however, that all rules, regulations, and ordinances of this Barangay are being observed
                and complied with accordingly
            </p>
            @break

        {{-- Purok Clearance --}}
        @case(7)
            <p class="indent">
                <b>THIS IS TO CERTIFY that I officially know <span class="underline">{{ $document->resident->fullname }}</span></b>, 
                resident of Barangay San Bernabe, Maddela, Quirino. Has no pending obligation in our Purok.
            </p>
            <p class="indent">He/she can now secure Barangay Clearance.</p>
            @break

        {{-- Certification --}}
        @case(8)
            <p class="indent">
                <b>THIS IS TO CERTIFY that I officially know <span class="underline">{{ $document->resident->fullname }}</span></b>,
                of legal age, {{ strtolower($document->resident->civil_status_str) }} and a resident of Barangay San Bernabe, Maddela, Quirino.
                This is to certify further the he is a legal owner of a <span class="underline">{{ $document->residentAsset->details[0]['value'] }}</span>
                with the following description:
            </p>
            <p>
                @foreach ($document->residentAsset->details as $detail)
                @if (!$loop->first)
                    <div class="indent uppercase">{{ $detail['label'] }}: <b>{{ $detail['value'] }}</b></div>
                @endif
                @endforeach
            </p>
            <p class="indent">{{ $document->purpose }}</p>
            <p class="indent">This certification is issued upon the request of the above mentioned name for whatever legal purpose it rnay deemed serve.</p>
            @break

        {{-- Other Certificates / Clearance --}}
        @default
            <p class="indent">
                <b>THIS IS TO CERTIFY that I officially know <span class="underline">{{ $document->resident->fullname }}</span></b>,
                of legal age, {{ strtolower($document->resident->civil_status_str) }} and a resident of Barangay San Bernabe, Maddela, Quirino.
                This is to certify further that he/she is of good moral character and a law abiding citizen in this community.
            </p>
            @switch($document->type)
                {{-- Barangay Clearance --}}
                @case(3)
                    <p class="indent">Records of this office show that as to this date he/she has not been accuse of any crime or any pending obligation.</p>
                    <p class="indent">This clearance is issued upon the request of the above-mentioned name for whatever legal purpose it may deem serve.</p>
                    @break

                {{-- Barangay Idigency --}}
                @case(4)
                    <p class="indent">Further certify that his/her family belongs to the indigents in this community.</p>
                    <p class="indent">This clearance is issued upon the request of the above-mentioned name for whatever legal purpose it may deem serve.</p>
                    @break

                {{-- Barangay Certificate --}}
                @default
                    <p class="indent">This certificate is issued upon the request of the above-mentioned name for whatever legal purpose it may deem serve.</p>
            @endswitch
    @endswitch

    {{-- Date Issued --}}
    @include('printables.document_sections.date_issued')

    {{-- Official Signatories --}}
    @include('printables.document_sections.signatories')

    {{-- Numbers and Date --}}
    @include('printables.document_sections.document_numbers')
@endsection