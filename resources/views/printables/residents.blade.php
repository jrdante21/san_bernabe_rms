@extends('layouts.print')
@section('title', 'Resident Lists')
@section('heading', 'Resident Lists')
@section('content')
    @if ($groupByPurok)
        @foreach ($residents as $purok => $lists)
            <p class="mb-5px"><b>Purok {{ $purok }}</b></p>
            @include('printables.tables.residents', ['residents' => $lists])
        @endforeach
    @else
        @include('printables.tables.residents')
    @endif
@endsection