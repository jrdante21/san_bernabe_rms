import { createApp } from 'vue'
import { createRouter, createWebHistory } from 'vue-router'
import { createStore } from 'vuex'
import axios from 'axios'
import naive from '~/naive.js'
import App from '~/components/App.vue'
import AppLayout from '~/components/admin/AppLayout.vue'
import ProfileDescription from '~/components/utils/ProfileDescription.vue'

const router = createRouter({
    history: createWebHistory(),
    routes: [
        {
            path: '',
            name: 'login',
            component: () => import(/* webpackChunkName: "js/webpack/login" */  '~/components/admin/Login.vue'),
        },{
            path: '/admin',
            component: AppLayout,
            children: [
                {
                    path: '',
                    name: 'home',
                    component: () => import(/* webpackChunkName: "js/webpack/home" */  '~/components/admin/Home.vue'),
                },{
                    path: 'residents',
                    name: 'residents',
                    component: () => import(/* webpackChunkName: "js/webpack/residents" */  '~/components/admin/Residents.vue'),
                },{
                    path: 'resident/:id',
                    name: 'resident',
                    props: true,
                    component: () => import(/* webpackChunkName: "js/webpack/residents" */  '~/components/admin/Resident.vue'),
                },{
                    path: 'transaction-document/:type',
                    name: 'transaction-document',
                    props: true,
                    component: () => import(/* webpackChunkName: "js/webpack/transaction" */  '~/components/admin/TransactionDocument.vue'),
                },{
                    path: 'transaction-summon',
                    name: 'transaction-summon',
                    component: () => import(/* webpackChunkName: "js/webpack/transaction" */  '~/components/admin/TransactionSummon.vue'),
                },{
                    path: 'transaction-material',
                    name: 'transaction-material',
                    component: () => import(/* webpackChunkName: "js/webpack/transaction" */  '~/components/admin/TransactionMaterial.vue'),
                },{
                    path: 'officials',
                    name: 'officials',
                    component: () => import(/* webpackChunkName: "js/webpack/officials" */  '~/components/admin/Officials.vue'),
                    beforeEnter: () => {
                        if (window.$user.role == 'official') return {name:'home'}
                    }
                },{
                    path: 'administrators',
                    name: 'administrators',
                    component: () => import(/* webpackChunkName: "js/webpack/admins" */  '~/components/admin/Admins.vue'),
                    beforeEnter: () => {
                        if (!window.$user.is_super) return {name:'home'}
                    }
                }
            ]
        }
    ]
})
router.beforeEach((to) => {
    var name = ''
    switch (to.name) {
        case 'transaction-document':
            name = window.$documentTypes[to.params.type]
            break;

        case 'transaction-summon':
            name = 'Summons'
            break;
    
        case 'transaction-material':
            name = 'Borrowed Materials'
            break;
        
        default:
            name = to.name.split('-').map(e => e.charAt(0).toUpperCase() + e.slice(1)).join(' ')
            break;
    }

    document.title = 'San Bernabe RMS | '+name
})

const store = createStore({
    state() {
        return {
            address: [],
            user: window.$user
        }
    },
    mutations: {
        setAddress(state, data) {
            state.address = data
        },
        setUser(state, data) {
            state.user = data
        }
    }
})

const API = axios.create({
    baseURL: '/api-admin',
    headers: {
        'Access-Control-Allow-Origin': '*',
        'Content-Type': 'application/json',
    },
})

API.interceptors.response.use(r => { return r }, error => {
    switch (error.response.status) {
        case 401:
            window.location.replace('/')
            break;
    
        case 403:
            window.$messageAlert.error("You can't do this action.", "Unauthorized action")
            break;

        case 500:
            window.$messageAlert.error("Try again later.", "Something went wrong")
            break;
    }

    return Promise.reject(error)
})
window.$api = API

const app = createApp(App)
app.component('ProfileDescription', ProfileDescription)
app.use(naive)
app.use(store)
app.use(router)
app.mount('#app')