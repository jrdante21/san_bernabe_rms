<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\API\ResidentController;
use App\Http\Controllers\API\ResidentEducationController;
use App\Http\Controllers\API\ResidentChildrenController;
use App\Http\Controllers\API\ResidentBusinessController;
use App\Http\Controllers\API\ResidentJobController;
use App\Http\Controllers\API\ResidentAssetController;
use App\Http\Controllers\API\ResidentSiblingController;
use App\Http\Controllers\API\TransactionDocumentController;
use App\Http\Controllers\API\TransactionSummonController;
use App\Http\Controllers\API\TransactionMaterialController;
use App\Http\Controllers\API\OfficialController;
use App\Http\Controllers\API\OfficialSignatoryController;
use App\Http\Controllers\API\UserController;
use App\Http\Controllers\API\AdminController;

use App\Http\Controllers\Prints\DocumentController as PrintDocument;
use App\Http\Controllers\Prints\TransactionController as PrintTransaction;
use App\Http\Controllers\Prints\ResidentController as PrintResident;

use App\Http\Controllers\LoginController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [LoginController::class, 'index'])->name('login');
Route::post('/login', [LoginController::class, 'login']);
Route::get('/logout', [LoginController::class, 'logout']);

Route::middleware('auth:web,official')->group(function(){
    Route::view('/admin/{any?}', 'admin')->where('any','.*')->name('admin');

    Route::prefix('print')->group(function(){
        Route::get('/documents', PrintDocument::class);
        Route::get('/transactions', PrintTransaction::class);
        Route::resource('residents', PrintResident::class)->only(['index','show']);
    });
});

Route::prefix('api-admin')->middleware('auth:web,official')->group(function(){
    Route::post('/resident-family/{resident}', [ResidentController::class, 'updateFamily']);
    Route::post('/resident-photo/{resident}', [ResidentController::class, 'updatePhoto']);
    Route::post('/admin-activate/{admin}', [AdminController::class, 'restore']);
    Route::post('/official-signatories', OfficialSignatoryController::class);

    Route::apiResource('admins', AdminController::class);
    Route::apiResource('officials', OfficialController::class);
    Route::apiResource('residents', ResidentController::class);
    Route::apiResource('residents.businesses', ResidentBusinessController::class)->except(['show'])->shallow();
    Route::apiResource('residents.assets', ResidentAssetController::class)->except(['show'])->shallow();
    Route::apiResource('residents.educations', ResidentEducationController::class)->except(['show','index'])->shallow();
    Route::apiResource('residents.children', ResidentChildrenController::class)->except(['show','index'])->shallow();
    Route::apiResource('residents.jobs', ResidentJobController::class)->except(['show','index'])->shallow();
    Route::apiResource('residents.siblings', ResidentSiblingController::class)->except(['show','index'])->shallow();
    Route::apiResource('transaction-documents', TransactionDocumentController::class);
    Route::apiResource('transaction-summons', TransactionSummonController::class);
    Route::apiResource('transaction-materials', TransactionMaterialController::class);

    Route::prefix('users')->group(function(){
        Route::post('/update-username', [UserController::class, 'updateUsername']);
        Route::post('/update-password', [UserController::class, 'updatePassword']);
    });
});