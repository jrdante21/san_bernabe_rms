<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;
use Illuminate\Database\Eloquent\Relations\Relation;
use App\Constants\MyConstants;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        View::share('documentTypes', MyConstants::DOCUMENT_TYPES);
        View::share('officialPositions', MyConstants::OFFCIAL_POSITIONS);

        Relation::enforceMorphMap([
            'document' => 'App\Models\TransactionDocument',
            'material' => 'App\Models\TransactionMaterial',
            'summon' => 'App\Models\TransactionSummon',
        ]);

        // $this->app->bind('path.public', function() {
        //     return base_path().'/..';
        // });
    }
}
