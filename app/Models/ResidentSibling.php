<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class ResidentSibling extends Model
{
    use HasFactory;
    public $table = 'resident_siblings';
    public $timestamps = false;

    protected $fillable = ['fname', 'mname', 'lname'];
    protected $appends = ['fullname'];

    public function getFullnameAttribute()
    {
        return Str::title(trim("{$this->fname} {$this->mname} {$this->lname}"));
    }
}
