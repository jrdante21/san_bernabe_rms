<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TransactionMaterial extends Model
{
    use HasFactory;

    protected $fillable = ['resident_id','material','status','quantity','amount','returned_at','paid_at','issued_at','or_number'];
    protected $appends = ['title','remarks'];
    protected $casts = [
        'amount' => 'float'
    ];

    public function getTitleAttribute()
    {
        return 'Borrowed Materials';
    }

    public function getRemarksAttribute () {
        $status = 'error';
        $message = "Not yet returned";

        switch ($this->status) {
            case 1:
                if (!empty($this->returned_at)) {
                    $status = 'success';
                    $message = "Returned";
                } else {
                    $status = 'warning';
                    $message = "Not yet returned";
                }
                break;

            case 2:
                if (!empty($this->paid_at) && !empty($this->returned_at)) {
                    $status = 'success';
                    $message = "Paid & Returned";
                } else {
                    $status = 'error';
                    $message = "Unpaid / Unreturned";
                }
                break;

            default:
                if (!empty($this->paid_at)) {
                    $status = 'success';
                    $message = "Paid";
                } else {
                    $status = 'error';
                    $message = "Unpaid";
                }
                break;
        }

        return compact('message','status');
    }

    public function resident()
    {
        return $this->belongsTo(Resident::class);
    }
    
    public function receipt()
    {
        return $this->morphOne(Receipt::class, 'receiptable');
    }
}