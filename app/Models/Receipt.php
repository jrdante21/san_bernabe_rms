<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Receipt extends Model
{
    use HasFactory;

    protected $fillable = ['resident_id'];

    public function receiptable()
    {
        return $this->morphTo();
    }

    public function resident()
    {
        return $this->belongsTo(Resident::class);
    }
}
