<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ResidentJob extends Model
{
    use HasFactory;
    public $table = 'resident_jobs';
    public $timestamps = false;

    protected $fillable = ['position','company_name','started_at','is_present','ended_at'];
    protected $appends = ['date_range'];
    protected $casts = ['is_present' => 'boolean'];

    public function getDateRangeAttribute()
    {
        $dateStarted = date('F Y', strtotime($this->started_at));
        $dateEnded = ($this->is_present) ? 'Present': date('F Y', strtotime($this->ended_at));
        return "{$dateStarted} - {$dateEnded}";
    }
}
