<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class ResidentBusiness extends Model
{
    use HasFactory;
    public $table = 'resident_businesses';
    public $timestamps = false;

    protected $fillable = ['name','address'];
    protected $appends = ['address_str'];
    protected $casts = ['address' => 'array'];

    public function getAddressStrAttribute()
    {
        $address = $this->address;
        return Str::title("{$address['barangay']}, {$address['municipality']}, {$address['province']}");
    }

    public function transactionDocument()
    {
        return $this->hasOne(TransactionDocument::class);
    }
}
