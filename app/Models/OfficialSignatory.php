<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OfficialSignatory extends Model
{
    use HasFactory;
    public $timestamps = false;

    protected $fillable = ['official_id','type'];

    public function official()
    {
        return $this->belongsTo(Official::class);
    }
}
