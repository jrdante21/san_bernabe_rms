<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    use HasFactory;

    public function getLocationsAttribute($value)
    {
        $addr = [];
        foreach (json_decode($value, true) as $v) {
            $addr[] = $v['province_list'];
        }
        return collect($addr)->collapse()->all();
    }

    public function scopeGetPhAddress($query)
    {
        return $query->find(1)->locations;
    }
}
