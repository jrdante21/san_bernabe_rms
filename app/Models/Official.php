<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Str;
use App\Constants\MyConstants;

class Official extends Authenticatable
{
    use HasFactory, Notifiable;

    protected $hidden = ['password'];
    protected $fillable = ['fname','mname','lname','title','position','username','password'];
    protected $appends = ['fullname','position_str','role'];

    public function getFullnameAttribute()
    {
        return Str::title(trim("{$this->title} {$this->fname} {$this->mname} {$this->lname}"));
    }

    public function getPositionStrAttribute()
    {
        $positions = MyConstants::OFFCIAL_POSITIONS;
        return (empty($positions[$this->position])) ? 'None' : $positions[$this->position];
    }

    public function getRoleAttribute()
    {
        return 'official';
    }

    public function scopeSearchByName($query, $search='')
    {
        if (empty($search)) return $query;
        $search = '%'.$search.'%';
        return $query
        ->whereRaw("
            CONCAT(fname, ' ', mname, ' ', lname) LIKE ? OR
            CONCAT(fname, ' ', lname) LIKE ? OR
            CONCAT(lname, ' ', fname) LIKE ?
        ", [$search, $search, $search]);
    }

    public function signatories()
    {
        return $this->hasMany(OfficialSignatory::class);
    }
}
