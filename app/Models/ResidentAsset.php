<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class ResidentAsset extends Model
{
    use HasFactory;
    public $table = 'resident_assets';
    public $timestamps = false;

    protected $fillable = ['type','descriptions'];
    protected $appends = ['type_str','details','details_str'];
    protected $casts = ['descriptions' => 'array'];

    public function getTypeStrAttribute()
    {
        switch ($this->type) {
            case 1: return 'Vehicle'; break;
            case 2: return 'Animal'; break;
            default: return 'Land'; break;
        }
    }

    public function getDetailsAttribute()
    {
        $desc = $this->descriptions;
        switch ($this->type) {
            case 1:
                return [
                    [ 'label'=>'Brand', 'value'=>$desc['brand'] ],
                    [ 'label'=>'Model', 'value'=>$desc['model'] ],
                    [ 'label'=>'Plate Number', 'value'=>$desc['plate_number'] ],
                    [ 'label'=>'Color', 'value'=>$desc['color'] ],
                ];
                break;

            case 2:
                return [
                    [ 'label'=>'Animal Type', 'value'=>$desc['animal_type'] ],
                    [ 'label'=>'Sex', 'value'=>($desc['sex'] <= 1) ? 'Male' : 'Female' ],
                    [ 'label'=>'Age', 'value'=>$desc['age'] ],
                    [ 'label'=>'Color', 'value'=>$desc['color'] ],
                ];
                break;

            case 3:
                return [
                    [ 'label'=>'Land Type', 'value'=>$desc['land_type'] ],
                    [ 'label'=>'Hectars', 'value'=>$desc['hectars'] ],
                    [ 'label'=>'Location', 'value'=>Str::title("{$desc['barangay']}, {$desc['municipality']}, {$desc['province']}") ],
                ];
                break;

            default: return []; break;
        }
    }

    public function getDetailsStrAttribute()
    {
        $details = collect($this->details)->map(fn($item) => "{$item['label']}: {$item['value']}")->all();
        return implode(', ', $details);
    }
    
    public function transactionDocument()
    {
        return $this->hasOne(TransactionDocument::class);
    }
}
