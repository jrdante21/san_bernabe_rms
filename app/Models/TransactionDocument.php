<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Constants\MyConstants;

class TransactionDocument extends Model
{
    use HasFactory;

    protected $fillable = ['resident_id','resident_business_id','resident_asset_id','type','ctc_number','amount','issued_at','purpose','or_number'];
    protected $appends = ['title'];
    protected $casts = [
        'amount' => 'float'
    ];

    public function getTitleAttribute()
    {
        $types = MyConstants::DOCUMENT_TYPES;
        return (empty($types[$this->type])) ? 'None' : $types[$this->type];
    }

    public function resident()
    {
        return $this->belongsTo(Resident::class);
    }

    public function residentBusiness()
    {
        return $this->belongsTo(ResidentBusiness::class);
    }

    public function residentAsset()
    {
        return $this->belongsTo(ResidentAsset::class);
    }

    public function receipt()
    {
        return $this->morphOne(Receipt::class, 'receiptable');
    }
}
