<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TransactionSummon extends Model
{
    use HasFactory;

    protected $fillable = ['resident_id','respondent_id','case_number','amount','issued_at','reason','scheduled_at','or_number'];
    protected $appends = ['title'];
    protected $casts = [
        'amount' => 'float'
    ];

    public function getTitleAttribute()
    {
        return 'Summons';
    }

    public function resident()
    {
        return $this->belongsTo(Resident::class);
    }

    public function respondent()
    {
        return $this->belongsTo(Resident::class, 'respondent_id');
    }

    public function receipt()
    {
        return $this->morphOne(Receipt::class, 'receiptable');
    }
}
