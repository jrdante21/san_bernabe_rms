<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class CheckUserRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next, $role='')
    {
        $user = (auth()->check()) ? auth()->user() : auth()->guard('official')->user();
        $authorized = true;
        
        switch ($role) {
            case 'superadmin':
                $authorized = $user->role == 'superadmin';
                break;

            case 'admin':
                $authorized = in_array($user->role, ['admin','superadmin']);
                break;
        }

        abort_if(!$authorized, 403, 'Unauthorized action.');

        $request->merge(['user' => $user]);
        return $next($request);
    }
}
