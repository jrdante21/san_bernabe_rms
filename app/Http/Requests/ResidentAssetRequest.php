<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ResidentAssetRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type' => ['required', 'numeric', 'min:1'],
            
            // Type 1
            'brand' => ['required_if:type,1', 'min:3'],
            'model' => ['required_if:type,1', 'min:3'],
            'plate_number' => ['required_if:type,1', 'min:3'],

            // Type 2
            'animal_type' => ['required_if:type,2', 'min:3'],
            'sex' => ['required_if:type,2', 'numeric', 'min:1'],
            'age' => ['required_if:type,2'],
            'certificate' => ['required_if:type,2', 'min:3'],

             // Type 1 and 2
            'color' => ['required_if:type,1||2', 'min:3'],

            // Type 3
            'land_type' => ['required_if:type,3', 'min:3'],
            'hectars' => ['required_if:type,3'],
            'barangay' => ['required_if:type,3', 'min:5'],
            'municipality' => ['required_if:type,3', 'min:5'],
            'province' => ['required_if:type,3', 'min:5'],
        ];
    }

    protected function prepareForValidation()
    {
        $this->merge([
            'barangay' => strtoupper($this->barangay),
            'municipality' => strtoupper($this->municipality),
            'province' => strtoupper($this->province),
        ]);
    }

    public function withValidator($validator)
    {
        $asset = $this->route('asset');
        if (empty($asset)) return;

        $transaction = $asset->transactionDocument;
        $validator->after(function ($validator) use ($transaction) {
            if (!empty($transaction)) {
                $validator->errors()->add('has_transaction', 'Asset already been used in transactions, cannot be modified.');
            }
        });
    }
}
