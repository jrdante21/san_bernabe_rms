<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use App\Models\ResidentBusiness;

class TransactionDocumentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'resident_id' => ['required', 'exists:residents,id'],
            'type' => ['required', 'numeric', 'min:1', 'max:8'],
            'ctc_number' => ['required', 'min:3'],
            'amount' => ['required', 'numeric', 'min:1'],
            'issued_at' => ['required', 'date'],
            'or_number' => ['required', 'min:3'],

            // Business
            'is_business' => ['required', 'boolean'],
            'resident_business_id' => ['exclude_if:is_business,false', 'required',
                Rule::exists('resident_businesses', 'id')->where(fn($query) => $query->where('resident_id', request()->input('resident_id')))
            ],

            // Asset
            'is_asset' => ['required', 'boolean'],
            'resident_asset_id' => ['exclude_if:is_asset,false', 'required',
                Rule::exists('resident_assets', 'id')->where(fn($query) => $query->where('resident_id', request()->input('resident_id')))
            ],
            'purpose' => ['exclude_if:is_asset,false', 'required', 'min:5'],
        ];
    }

    protected function prepareForValidation()
    {
        $this->merge([
            'issued_at' => date('Y-m-d', strtotime($this->issued_at)),
            'is_business' => in_array($this->type, [5,6]),
            'is_asset' => $this->type == 8,
        ]);
    }

    public function attributes()
    {
        return [
            'resident_id' => 'resident',
        ];
    }
}
