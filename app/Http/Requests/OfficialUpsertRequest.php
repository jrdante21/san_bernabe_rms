<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use App\Rules\AlphaSpace;

class OfficialUpsertRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $official = $this->route('official');

        return [
            'fname' => ['required', 'min:2', new AlphaSpace],
            'mname' => ['required', 'min:2', new AlphaSpace],
            'lname' => ['required', 'min:2', new AlphaSpace],
            'title' => ['required'],
            'position' => ['required', 'numeric', 'min:1'],
            'username' => ['required', 'min:5', 'alpha_num', Rule::unique('officials', 'username')->ignore($official)],
            'edit_password' => ['required', 'boolean'],
            'password' => ['exclude_if:edit_password,false', 'required', 'min:5'],
        ];
    }

    protected function prepareForValidation()
    {
        $official = $this->route('official');
        $this->merge([
            'edit_password' => (!empty($official) && empty($this->password)) ? false : true
        ]);
    }
}
