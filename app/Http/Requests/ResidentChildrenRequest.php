<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Rules\AlphaSpace;

class ResidentChildrenRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'fname' => ['required', 'min:2', new AlphaSpace],
            'mname' => ['required', 'min:2', new AlphaSpace],
            'lname' => ['required', 'min:2', new AlphaSpace],
            'gender' => ['required', 'numeric', 'min:1'],
            'bday' => ['required', 'date'],
            'civil_status' => ['required', 'numeric', 'min:1'],
            'is_student' => ['required', 'boolean'],
            'school_level' => ['exclude_if:is_student,false', 'required_if:is_student,true', 'numeric', 'min:1'],
            'school_name' => ['exclude_if:is_student,false', 'required_if:is_student,true', 'min:5'],
            'school_address.province' => ['exclude_if:is_student,false', 'required_if:is_student,true', 'min:3'],
            'school_address.municipality' => ['exclude_if:is_student,false', 'required_if:is_student,true', 'min:3'],
            'school_address.barangay' => ['exclude_if:is_student,false', 'required_if:is_student,true', 'min:3'],
        ];
    }

    protected function prepareForValidation()
    {
        $this->merge([
            'bday' => date('Y-m-d', strtotime($this->bday)),
            'school_address.barangay' => strtoupper($this->school_address['barangay']),
            'school_address.municipality' => strtoupper($this->school_address['municipality']),
            'school_address.province' => strtoupper($this->school_address['province']),
        ]);
    }
}
