<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TransactionMaterialRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'resident_id' => ['required', 'exists:residents,id'],
            'material' => ['required', 'min:2'],
            'status' => ['required', 'numeric', 'min:1', 'max:3'],
            'quantity' => ['required', 'numeric', 'min:1'],
            'amount' => ['required', 'numeric'],
            'returned_at' => ['nullable', 'date'],
            'paid_at' => ['nullable', 'date'],
            'issued_at' => ['required', 'date'],
            'or_number' => ['required', 'min:3']
        ];
    }

    protected function prepareForValidation()
    {
        $this->merge([
            'returned_at' => (!is_null($this->returned_at)) ? date('Y-m-d', strtotime($this->returned_at)) : null,
            'paid_at' => (!is_null($this->paid_at)) ? date('Y-m-d', strtotime($this->paid_at)) : null,
            'issued_at' => date('Y-m-d', strtotime($this->issued_at)),
            'material' => ucwords($this->material)
        ]);
    }
}
