<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TransactionSummonRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'resident_id' => ['required', 'exists:residents,id'],
            'respondent_id' => ['required', 'different:resident_id', 'exists:residents,id'],
            'case_number' => ['required', 'min:3'],
            'amount' => ['required', 'numeric', 'min:1'],
            'scheduled_at' => ['required', 'date'],
            'issued_at' => ['required', 'date'],
            'reason' => ['exclude_if:is_asset,false', 'required', 'min:5'],
            'or_number' => ['required', 'min:3']
        ];
    }

    protected function prepareForValidation()
    {
        $this->merge([
            'issued_at' => date('Y-m-d', strtotime($this->issued_at)),
            'scheduled_at' => date('Y-m-d H:i:00', strtotime($this->scheduled_at))
        ]);
    }

    public function attributes()
    {
        return [
            'resident_id' => 'resident',
            'respondent_id' => 'respondent',
        ];
    }
}
