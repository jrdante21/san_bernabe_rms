<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Resident;
use App\Models\ResidentSibling;
use App\Http\Requests\ResidentSiblingRequest;

class ResidentSiblingController extends Controller
{
    public function __construct()
    {
        $this->middleware('user:admin')->except(['index']);
    }

    public function store(ResidentSiblingRequest $request, Resident $resident)
    {
        $valid = $request->validated();
        $resident->siblings()->create($valid);
        return $resident->siblings;
    }

    public function update(ResidentSiblingRequest $request, ResidentSibling $sibling)
    {
        $valid = $request->validated();
        $sibling->update($valid);
        return $sibling;
    }

    public function destroy(ResidentSibling $sibling)
    {
        $sibling->delete();
    }
}
