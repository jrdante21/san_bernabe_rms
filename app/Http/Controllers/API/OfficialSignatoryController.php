<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

use App\Models\OfficialSignatory;

class OfficialSignatoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('user:admin');
    }

    public function __invoke(Request $request)
    {
        $types = ['summon'];
        for ($i=2; $i <= 8; $i++) { 
            $types[] = 'document_'.$i;
        }

        $valid = $request->validate([
            'type' => ['required', Rule::in($types)],
            'officials' => ['required','array'],
        ]);

        $officials = collect($valid['officials'])
            ->map(fn($item) => [
                'type' => $valid['type'],
                'official_id' => $item
            ])
            ->all();
        
        OfficialSignatory::where('type', $valid['type'])->delete();
        $signatories = OfficialSignatory::insert($officials);
        return $signatories;
    }
}
