<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\TransactionSummon;
use App\Http\Requests\TransactionSummonRequest;

class TransactionSummonController extends Controller
{
    public function __construct()
    {
        $this->middleware('user:admin')->except(['index']);
    }

    public function index(Request $request)
    {
        $transactionSummons = TransactionSummon::with(['resident'])
            ->when($request->search, function($query, $search){
                return $query->whereHas('resident', fn($q) => $q->searchByName($search));
            })
            ->with('respondent')
            ->latest()
            ->paginate(20);
        return $transactionSummons;
    }

    public function store(TransactionSummonRequest $request)
    {
        $valid = $request->validated();
        $transactionSummon = TransactionSummon::create($valid);
        $transactionSummon->receipt()->create(['resident_id' => $transactionSummon->resident_id]);
        return $transactionSummon;
    }

    public function update(TransactionSummonRequest $request, TransactionSummon $transactionSummon)
    {
        $valid = $request->validated();
        $transactionSummon->update($valid);
        return $transactionSummon;
    }
}
