<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\TransactionDocument;
use App\Http\Requests\TransactionDocumentRequest;

class TransactionDocumentController extends Controller
{
    public function __construct()
    {
        $this->middleware('user:admin')->except(['index']);
    }

    public function index(Request $request)
    {
        $type = $request->input('type', 1);
        $transactionDocuments = TransactionDocument::with(['resident'])
            ->when($request->search, function($query, $search){
                return $query->whereHas('resident', fn($q) => $q->searchByName($search));
            })
            ->where('type', $type)
            ->when(in_array($type, [5,6]), fn($query) => $query->with('residentBusiness'))
            ->when($type == 8, fn($query) => $query->with('residentAsset'))
            ->latest()
            ->paginate(20);
        return $transactionDocuments;
    }

    public function store(TransactionDocumentRequest $request)
    {
        $valid = $request->validated();
        $transactionDocument = TransactionDocument::create($valid);
        $transactionDocument->receipt()->create(['resident_id' => $transactionDocument->resident_id]);
        return $transactionDocument;
    }

    public function update(TransactionDocumentRequest $request, TransactionDocument $transactionDocument)
    {
        $valid = $request->validated();
        $transactionDocument->update($valid);
        return $transactionDocument;
    }
}
