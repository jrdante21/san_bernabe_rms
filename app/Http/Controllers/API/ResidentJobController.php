<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Resident;
use App\Models\ResidentJob;
use App\Http\Requests\ResidentJobRequest;

class ResidentJobController extends Controller
{
    public function __construct()
    {
        $this->middleware('user:admin');
    }

    public function store(ResidentJobRequest $request, Resident $resident)
    {
        $valid = $request->validated();
        $resident->jobs()->create($valid);
        return $resident->jobs;
    }

    public function update(ResidentJobRequest $request, ResidentJob $job)
    {
        $valid = $request->validated();
        $job->update($valid);
        return $job;
    }

    public function destroy(ResidentJob $job)
    {
        $job->delete();
    }
}
