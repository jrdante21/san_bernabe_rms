<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Rules\Base64Image;
use App\Models\Resident;
use App\Http\Requests\ResidentRequest;
use App\Http\Requests\FullnameRequest;

class ResidentController extends Controller
{
    public function __construct()
    {
        $this->middleware('user:admin')->except(['index','show']);
    }

    public function index(Request $request)
    {
        $residents = Resident::when($request->search, fn($query, $search) => $query->searchByName($search))
            ->latest()
            ->paginate(20);
        return $residents;
    }

    public function show(Resident $resident)
    {  
        $resident->educations;
        $resident->children;
        $resident->businesses;
        $resident->jobs;
        $resident->assets;
        $resident->siblings;
        return $resident;
    }

    public function store(ResidentRequest $request)
    {
        $valid = $request->validated();
        $resident = Resident::create($valid);
        return $resident;
    }

    public function update(ResidentRequest $request, Resident $resident)
    {
        $valid = $request->validated();
        $resident->update($valid);
        return $resident;
    }

    public function updateFamily(FullnameRequest $request, Resident $resident)
    {
        $valid = $request->validated();
        switch ($request->type) {
            case 'father':
                $update['father_name'] = $valid;
                break;

            case 'mother':
                $update['mother_name'] = $valid;
                break;
            
            case 'spouse':
                $update['spouse_name'] = $valid;
                break;
                
            default:
                abort(404);
                break;
        }

        $resident->update($update);
        return $resident;
    }

    public function updatePhoto(Request $request, Resident $resident)
    {
        $valid = $request->validate([
            'photo' => ['required', new Base64Image]
        ]);

        $photo = explode(';base64', $valid['photo']);
        $photoType = explode('image/', $photo[0])[1];
        $photoBase64 = base64_decode($photo[1]);
        $photoName = time().'.'.$photoType;

        file_put_contents('images/profiles/'.$photoName, $photoBase64);

        // Delete previous photo
        if (!empty($resident->photo)) {
            $photoOld = 'images/profiles/'.$resident->photo;
            chown($photoOld, 666);
            unlink($photoOld);
        }

        $resident->photo = $photoName;
        $resident->save();
        return $resident->photo;
    }
}
