<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('user');
    }
    
    public function updateUsername(Request $request)
    {
        $valid = $request->validate(['username' => 'required|alpha_num|min:5']);
        $request->user->update($valid);
        return $request->user;
    }

    public function updatePassword(Request $request)
    {
        $user = $request->user;
        $valid = $request->validate([
            'password_old' => [
                'required',
                function($attr, $value, $fail) use ($user) {
                    if (!Hash::check($value, $user->password)) $fail("The password is incorrect.");
                }],
            'password' => 'required|min:5|confirmed|different:password_old',
        ]);

        $user->update(['password' => bcrypt($valid['password'])]);
        return $user;
    }
}
