<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\TransactionMaterial;
use App\Http\Requests\TransactionMaterialRequest;

class TransactionMaterialController extends Controller
{
    public function __construct()
    {
        $this->middleware('user:admin')->except(['index']);
    }

    public function index(Request $request)
    {
        $transactionMaterials = TransactionMaterial::with(['resident'])
            ->when($request->search, function($query, $search){
                return $query->whereHas('resident', fn($q) => $q->searchByName($search));
            })
            ->latest()
            ->paginate(20);
        return $transactionMaterials;
    }

    public function store(TransactionMaterialRequest $request)
    {
        $valid = $request->validated();
        $transactionMaterial = TransactionMaterial::create($valid);
        $transactionMaterial->receipt()->create(['resident_id' => $transactionMaterial->resident_id]);
        return $transactionMaterial;
    }

    public function update(TransactionMaterialRequest $request, TransactionMaterial $transactionMaterial)
    {
        $valid = $request->validated();
        $transactionMaterial->update($valid);
        return $transactionMaterial;
    }
}
