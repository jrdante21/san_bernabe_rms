<?php

namespace App\Http\Controllers\Prints;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

use App\Models\OfficialSignatory;

class DocumentController extends Controller
{
    public function __invoke(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'id' => 'required',
            'type' => ['required', Rule::in(['document','summon'])]
        ]);
        if ($validator->fails()) abort(404);

        // Documents Or Summons
        $model = 'App\\Models\\'.Str::studly('transaction_'.strtolower($request->type));
        $document = $model::with(['resident','receipt'])->findOrFail($request->id);
        if ($request->type == 'document') {
            if ($document->type <= 1) abort(404);
            $document->residentBusiness;
            $document->residentAsset;
        } else {
            $document->respondent;
            $document->scheduled_at = now()->parse($document->scheduled_at);
        }

        $documentType = ($request->type == 'document') ? 'document_'.$document->type : 'summon';
        $signatories = OfficialSignatory::with('official')->where('type', $documentType)->get();

        $title = ($request->type == 'document' && !in_array($document->type, [2,3,4,8])) ? 'Barangay '.$document->title : $document->title;

        $view = ($request->type == 'document') ? 'document' : 'document_summon';
        return view('printables.'.$view, compact('document', 'signatories', 'title'));
    }
}
