<?php

namespace App\Http\Controllers\Prints;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Resident;

class ResidentController extends Controller
{
    public function index(Request $request)
    {
        $residents = Resident::when($request->with_assets, fn($query) => $query->with('assets'))->get();

        $withAssets = (!empty($request->with_assets)) ? true : false;
        $groupByPurok = (!empty($request->group_by_purok)) ? true : false; 
        if ($groupByPurok) $residents = collect($residents)->groupBy('purok');

        return view('printables.residents', compact('residents', 'withAssets', 'groupByPurok'));
    }

    public function show(Resident $resident)
    {
        $resident->educations;
        $resident->children;
        $resident->businesses;
        $resident->jobs;
        $resident->assets;
        return view('printables.resident', compact('resident'));
    }
}
