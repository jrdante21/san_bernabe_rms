<?php

namespace App\Http\Controllers\Prints;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Builder;

use App\Models\Receipt;
use App\Models\TransactionDocument;

class TransactionController extends Controller
{
    public function __invoke(Request $request)
    {
        $receipts = Receipt::with(['receiptable','resident'])
            ->when($request->resident_id, fn($query, $resident_id) => $query->where('resident_id', $resident_id))
            ->when($request->type, function($query, $type){
                if ($type == 'all') return $query;

                $type = explode('_', $type);
                $query->where('receiptable_type', $type[0]);
                if (!empty($type[1])) {
                    $query->whereHasMorph(
                        'receiptable',
                        TransactionDocument::class,
                        fn(Builder $query2) => $query2->where('type', $type[1])
                    );
                }
            })
            ->whereHasMorph('receiptable', '*', function(Builder $query) use ($request){
                return $query->whereRaw("DATE(issued_at) >= ? AND DATE(issued_at) <= ?",[
                    date('Y-m-d', strtotime($request->date_start)),
                    date('Y-m-d', strtotime($request->date_end))
                ]);
            })
            ->get();

        $total = collect($receipts)->reduce(fn($carry, $item) => $carry + $item->receiptable->amount, 0);

        return view('printables.transactions', compact('receipts','total'));
    }
}
